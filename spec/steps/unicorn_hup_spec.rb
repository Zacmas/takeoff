# frozen_string_literal: true

require 'spec_helper'
require './lib/lb/config'
require './lib/lb/command_wrapper'
require './lib/steps/unicorn_hup'

describe Steps::UnicornHup do
  before do
    enable_dry_run

    allow_any_instance_of(Lb::Config).to receive(:lb_list).and_return('lb-1:lb-2')
  end

  context 'base role' do
    subject do
      described_class.new(Roles.new('gstg'),
                          roles_to_hup: 'gstg-base-fe-web',
                          command_wrapper: Lb::CommandWrapper.new('gstg'))
    end

    it 'outputs the right command for the base fe web role' do
      command = <<~COMMAND
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl hup unicorn && sleep 3"
      COMMAND

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end
  end

  context 'array of roles' do
    subject do
      described_class.new(Roles.new('gstg'),
                          roles_to_hup: ['gstg-base-fe-web', 'gitlab2-base-fe-web'],
                          command_wrapper: Lb::CommandWrapper.new('gstg'))
    end

    it 'outputs the right command for an array of roles' do
      command = <<~COMMAND
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web OR roles:gitlab2-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-1:lb-2 -c "sudo gitlab-ctl hup unicorn && sleep 3"
      COMMAND

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end
  end
end
