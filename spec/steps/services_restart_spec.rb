# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/services_restart'
require './lib/lb/config'
require './lib/lb/command_wrapper'

describe Steps::ServicesRestart do
  let(:wrapper) { }

  before { enable_dry_run }

  context 'custom node' do
    subject do
      described_class.new(Roles.new('gstg'),
                          role: 'gstg-base-fe-web',
                          skip_services: ['gitlab-pages'],
                          command_wrapper: Lb::CommandWrapper.new('gstg'))
    end

    it 'outputs the right command' do
      allow_any_instance_of(Lb::Config).to receive(:lb_list).and_return('lb-01:lb-02')
      stub_const('Steps::ServicesRestart::CUSTOM_RESTART_NODE', 'gstg-base-fe-web')

      command = <<~COMMAND
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-01:lb-02 -c "sudo gitlab-ctl restart gitlab-workhorse && sleep 3"
        bundle exec knife ssh -e -C 3 -a fqdn roles:gstg-base-fe-web sudo /usr/local/bin/ha-ctl -l lb-01:lb-02 -c "sudo gitlab-ctl restart nginx && sleep 3"
      COMMAND

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end
  end

  context 'all other nodes' do
    subject { described_class.new(Roles.new('gstg'), role: 'gitlab-base-other') }

    it 'outputs the right command' do
      command = <<~COMMAND
        bundle exec knife ssh -e -a fqdn roles:gitlab-base-other sudo gitlab-ctl restart
      COMMAND

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end
  end
end
