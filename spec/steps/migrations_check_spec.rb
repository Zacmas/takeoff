# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/migrations_check'

describe Steps::MigrationsCheck do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('gstg')) }

  it 'outputs the right command' do
    command = <<~COMMAND
      bundle exec knife ssh -e -a fqdn roles:gstg-base-deploy-node sudo gitlab-rake db:migrate:status
    COMMAND

    expect { subject.run }.to output(/#{command}/).to_stdout_from_any_process
  end
end
