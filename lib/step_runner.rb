# frozen_string_literal: true

require './lib/steps/base_roles'
require './lib/slack_webhook'
require './lib/resumer'
require './lib/pid'

class StepRunner
  include SemanticLogger::Loggable

  def initialize(options)
    @version = options.delete(:version)
    @environment = options.delete(:env)
    @repo = options.delete(:repo)
    @options = options
    @resumer = Resumer.new(@options[:resume])
    @slack_webhook = SlackWebhook.new(@environment, @version, SharedStatus.new(@resumer.start_time))
    @queue = generate_queue
    
    validate_steps!(@options[:skip_steps])
    validate_steps!(@options[:run_steps])
  end

  def validate_steps!(steps = nil)
    return unless steps

    valid_steps = Takeoff.steps.flat_map(&:keys)
    
    steps.each do |step| 
      raise "`#{step}` is not a valid step" unless valid_steps.include?(step)
    end
  end

  def generate_queue
    Takeoff.steps.each_with_object([]).with_index do |(step, queue), step_number|
      step_name = step.keys.first

      next if @options[:skip_steps]&.include?(step_name)
      next if @options[:run_steps] && !@options[:run_steps].include?(step_name)
      next if !@options[:stop_sidekiq] && step_name.start_with?('sidekiq')
      next if @resumer.skip?(step_number)

      klass = Steps[step_name]

      queue << if klass < Steps::BaseRoles
                 klass.new(roles, step_options(step))
               else
                 klass.new(slack_webhook: @slack_webhook)
               end
    end
  end

  def run
    notify_start

    @queue.each do |step|
      @resumer.set_current_step(Steps.number(step.class))
      step.run!
    end

    notify_end

    Pid.destroy
  rescue ScriptError => e
    logger.error('Error during deployment', e)
    if @options[:auto_resume]
      rerun
    else
      Pid.destroy

      raise
    end
  end

  def roles
    @roles ||= Roles.new(@environment)
  end

  def step_options(step)
    value_array = step.values.flatten

    step_config.select { |key, _value| value_array.include?(key.to_s) }.tap do |options_hash|
      options_hash['pre_checks'] = value_array.first['pre_checks']
      options_hash['post_checks'] = value_array.first['post_checks']
      options_hash[:slack_webhook] = @slack_webhook
      options_hash[:resumer] = @resumer
    end
  end

  def step_config
    {
      version: @version,
      environment: @environment,
      repo: @repo,
      roles_to_start: roles.regular_and_blessed,
      resumer: @resumer,
      command_wrapper: command_wrapper
    }
  end

  private

  def command_wrapper
    @command_wrapper ||= Lb::CommandWrapper.new(@environment)
  end

  def notify_end
    @slack_webhook.deploy_finished
    @slack_webhook.qa_notification

    @resumer.success
  end

  def notify_start
    if @options[:resume] && @resumer.current_step > 0
      logger.info('Resuming deployment', step_number: @resumer.current_step)
      @slack_webhook.deploy_resumed
    else
      @slack_webhook.deploy_started
    end
  end

  def rerun
    begin
      puts ANSI::YELLOW % 'Auto-resuming in 10 seconds... (Press ^C to abort)'
      sleep(10)
      logger.info('Auto-resuming deploy', version: @version, environment: @environment)
    rescue Interrupt => e
      logger.warn('Canceled auto-resume', version: @version, environment: @environment)
      abort(ANSI::RED % 'Canceled auto-resume.')
    end

    @queue = generate_queue

    run
  end
end
