module Lb
  class CommandWrapper
    extend Forwardable

    SCRIPT_LOCATION = '/usr/local/bin/ha-ctl'.freeze

    def_delegators :@config, :has_lb?

    def initialize(environment)
      @environment = environment
      @config = Lb::Config.new(@environment)
    end

    def run_command(command:, role: nil)
      script_command(command: command, role: role)
    end

    def drain(role: nil)
      script_command(action: 'drain', role: role)
    end

    def maintenance(role: nil)
      script_command(action: 'maint', role: role)
    end

    def ready(role: nil)
      script_command(action: 'ready', role: role)
    end

    def stats
      server_query = "echo show stat | sudo socat stdio /run/haproxy/admin.sock | cut -d, -f1,2 | grep #{@environment} | sort"

      cmd = "bundle exec knife ssh #{Takeoff.fqdn_or_ipaddress} -p 2222 'roles:#{@environment}-base-lb' 'servers=$(#{server_query}); for server in $servers; do echo ${server},$(echo show stat | sudo socat stdio /run/haproxy/admin.sock | egrep \"^${server/\//,}\" | egrep -oh \"MAINT|UP|DRAIN\"); done' 2>/dev/null | sort | uniq -c"

      `#{cmd}`
    end

    private

    def script_command(command: nil, action: nil, role: nil)
      "sudo #{SCRIPT_LOCATION} -l #{@config.lb_list(role: role)}".tap do |cmd|
        cmd << " -a #{action}" if action
        cmd << " -c \"#{command} && sleep #{Takeoff.config[:knife_concurrency_wait]}\"" if command
      end.freeze
    end
  end
end
