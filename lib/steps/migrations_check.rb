# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class MigrationsCheck < BaseRoles
    def run
      return unless ran_migrations?

      stdout, = run_command_on_roles roles.blessed_node,
                                     'sudo gitlab-rake db:migrate:status',
                                     title: 'Checking for pending migrations'

      parse_pending_migrations_output(stdout)
    end

    private

    def parse_pending_migrations_output(output)
      found = []

      output.each_line do |line|
        status, migration = line.strip.split(/\s+/, 2)

        found << migration if status == 'down'
      end

      puts "The following migrations are pending:\n\n #{found}" unless found.empty?
    end

    def ran_migrations?
      roles.run_migrations
    end
  end
end
