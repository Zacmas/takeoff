# frozen_string_literal: true

require './lib/steps/migrations'

module Steps
  class PostMigrations < Migrations
    def run
      return unless run_post_deploy_migrations?
      super
    end

    protected

    def migrate_command
      'sudo gitlab-rake db:migrate'
    end

    def title
      'Running post-deployment migrations from the blessed node'
    end

    def type
      'post-deployment migrations'
    end

    def run_post_deploy_migrations?
      roles.run_post_deploy_migrations
    end
  end
end
