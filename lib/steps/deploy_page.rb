# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class DeployPage < BaseRoles
    def run
      run_command_on_roles deploy_page_roles,
                           'sudo gitlab-ctl start',
                           title: "Starting GitLab services on #{Roles.short_output(deploy_page_roles)}"

      run_command_on_roles deploy_page_roles,
                           'sudo gitlab-ctl deploy-page down',
                           title: "Disabling the deploy page on #{Roles.short_output(deploy_page_roles)}"
    end

    private

    def deploy_page_roles
      roles.service_roles['deploy-page']
    end
  end
end
